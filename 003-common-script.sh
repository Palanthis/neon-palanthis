#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://gitlab.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

# Kernel Build
sudo apt install linux-headers-$(uname -r) build-essential dkms

# Install Apps
sudo apt install -y screenfetch lolcat cairo-dock cairo-dock-plug-ins cryfs kate
sudo apt install -y qt5-style-kvantum keepassxc samba smb4k rhythmbox ffmpegthumbs
sudo apt install -y dolphin-plugins youtube-dl unrar simplescreenrecorder geany geany-plugins
sudo apt install -y flameshot kid3-qt audacity grsync dkms ccache htop kgpg ffmpeg yakuake
sudo apt install -y conky-all grub-customizer smbnetfs plasma-vault vlc clementine ktorrent

# Virtualization
sudo apt install -y qemu-kvm virt-manager

# Copy over some of my favorite fonts, themes and icons
sudo [ -d /usr/share/fonts/OTF ] || sudo mkdir /usr/share/fonts/OTF
sudo [ -d /usr/share/fonts/TTF ] || sudo mkdir /usr/share/fonts/TTF
sudo tar xzf tarballs/adobe-source-code-pro.tar.gz -C /usr/share/fonts/ --overwrite
sudo [ -d ~/.local/share/templates ] || sudo mkdir -p ~/.local/share/templates
sudo tar xzf tarballs/fonts-otf.tar.gz -C /usr/share/fonts/opentype/ --overwrite
sudo tar xzf tarballs/fonts-ttf.tar.gz -C /usr/share/fonts/truetype/ --overwrite
sudo tar xzf tarballs/Buuf-Plasma-1.7.tar.gz -C /usr/share/icons/ --overwrite
sudo tar xzf tarballs/buuf3.34.tar.gz -C /usr/share/icons/ --overwrite
sudo tar xzf tarballs/templates.tar.gz -C ~/.local/share/ --overwrite
sudo tar xzf tarballs/adapta.tar.gz -C /usr/share/themes/ --overwrite
sudo tar xzf tarballs/adapta-home.tar.gz -C ~/ --overwrite
sudo tar xzf tarballs/share.tar.gz -C /usr/share/ --overwrite


# Add screenfetch to .bashrc
echo 'screenfetch | lolcat' >> ~/.bashrc

# Add override for Kvantum
echo "export QT_STYLE_OVERRIDE=kvantum" >> ~/.profile

# Install Adapta KDE theme
sudo add-apt-repository ppa:papirus/papirus
sudo apt-get update
sudo apt-get install -y --install-recommends adapta-kde

echo " "
echo "All done!"
